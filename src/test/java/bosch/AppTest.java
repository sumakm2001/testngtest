package bosch;

import static org.testng.Assert.assertTrue;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

/**
 * Unit test for simple App.
 */
public class AppTest {
    @BeforeTest
  public void setUp() {
    System.out.println("This will be called before the test");
  }

  @Test(priority = 1)
  public void testMethod1() {
        System.out.println("Test Method 1");
	  assertTrue(true);
  }

  @Test(priority = 1)
  public void testMethod2() {
    System.out.println("Test Method 2");
	  assertTrue(true);
  }

  @Test(priority = 1)
  public void testMethod3() {
    System.out.println("Test Method 1");
	  assertTrue(true);
  }

  @AfterTest
	public void tearDown() {
		System.out.println("This will be called after the test");
	}
}
